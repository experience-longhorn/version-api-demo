using System;
using System.Text;
using System.Runtime.InteropServices;

namespace VersionApiDemo
{
	public class Program
	{
		// Taken from RtlGetOSProductName() flags in WinNT.h, from 4051 SDK
		[Flags]
		public enum OS_PRODUCTNAME : uint
		{
			OS_PRODUCTNAME_COMPANY			= 0x01,
			OS_PRODUCTNAME_FAMILY_GENERIC	= 0x02,
			OS_PRODUCTNAME_FAMILY_SPECIFIC	= 0x04,
			OS_PRODUCTNAME_TYPE				= 0x08,
			OS_PRODUCTNAME_VERSION			= 0x10,
			OS_PRODUCTNAME_SPVERSION		= 0x20,
			OS_PRODUCTNAME_COPYRIGHT		= 0x40
		}

		[DllImport("Kernel32.dll", CharSet = CharSet.Unicode, EntryPoint = "GetOSProductNameW")]
		public static extern int GetOSProductName(StringBuilder sb, uint length, OS_PRODUCTNAME returnType);

		[STAThread]
		public static void Main(string[] args)
		{
			for (uint i = 1; i <= 64; i *= 2)
			{
				OS_PRODUCTNAME part = (OS_PRODUCTNAME)i;
				Console.WriteLine("{0}:\r\n{1}\r\n", part, ReadPart(part));
			}
			Console.Read();
		}

		private static string ReadPart(OS_PRODUCTNAME part)
		{
			StringBuilder buff = new StringBuilder();
			buff.EnsureCapacity(255);
			GetOSProductName(buff, (uint)buff.Capacity, part);
			return buff.ToString();
		}
	}
}
